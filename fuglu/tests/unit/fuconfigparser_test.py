import unittest
import os

from configparser import NoOptionError
from fuglu.shared import FuConfigParser, BasicPlugin


class TestFuConfigParserString(unittest.TestCase):
    """Test for config wrapper extracting type string"""

    def test_existing_string(self):
        """Test if string variable given in config file is correctly returned"""
        config = FuConfigParser()

        configtext = """
        [test]
        var1 = test
        """
        config.read_string(configtext)

        var1 = config.get("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(var1, "test")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": "<undefined>",
                "description": "this is just a test for string var1"
            }
        })

        var1 = config.get("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(var1, "test")
        self.assertIsInstance(var1, str)

    def test_default_string(self):
        """Test if default string variable is returned if variable not defined"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.get("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": "<undefined>",
                "description": "this is just a test for string var1"
            }
        })

        var1 = config.get("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(var1, "<undefined>")
        self.assertIsInstance(var1, str)

    def test_default_None(self):
        """Test if default string variable is returned if variable not defined"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.get("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": None,
            }
        })

        var1 = config.get("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertIsNone(var1)

    def test_manual_fallback_string(self):
        """Test if explicit fallback string has precedence"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.get("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": "<undefined>",
                "description": "this is just a test for string var1"
            }
        })

        var1 = config.get("test", "var1", fallback="{{manual}}")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(var1, "{{manual}}")
        self.assertIsInstance(var1, str)

    def test_nodefault_exception_string(self):
        """Test if undefined or value without default raises exception"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.get("test", "var1")
        with self.assertRaises(NoOptionError):
            _ = config.get("test", "var2")

        config.update_defaults(section="test", defaults={
            "var1": {
                "description": "this is just a test for string var1"
            }
        })

        with self.assertRaises(NoOptionError):
            _ = config.get("test", "var2")
        with self.assertRaises(NoOptionError):
            _ = config.get("test", "var1")


class TestFuConfigParserInt(unittest.TestCase):
    """Test for config wrapper extracting type int"""

    def test_existing_int(self):
        """Test if int variable given in config file is correctly returned"""
        config = FuConfigParser()

        configtext = """
        [test]
        var1 = 10
        """
        config.read_string(configtext)

        var1 = config.getint("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(var1, 10)

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": -1,
                "description": "this is just a test for integer var1"
            }
        })

        var1 = config.getint("test", "var1")
        print(f"From FuFuConfigParser -> var1: {var1}")
        self.assertIsInstance(var1, int)
        self.assertEqual(var1, 10)

    def test_default_int(self):
        """Test if int variable given in config file is correctly returned"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getint("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": -1,
                "description": "this is just a test for integer var1"
            }
        })

        var1 = config.getint("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertIsInstance(var1, int)
        self.assertEqual(var1, -1)

    def test_default_realint(self):
        """
        Test if default int variable is returned if given as string

        Note:
        In the configuration file, an int can be given as string,
        so the string should be converted to an int. For the wrapper
        a string value has to work as well.
        """
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getfloat("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": "1",
                "description": "this is just a test for int var1"
            }
        })

        var1 = config.getint("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertIsInstance(var1, int)
        self.assertEqual(var1, 1)

    def test_manual_fallback_int(self):
        """Test if explicit fallback int has precedence"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getint("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": -1,
                "description": "this is just a test for int var1"
            }
        })

        var1 = config.getint("test", "var1", fallback=999)
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(999, var1)
        self.assertIsInstance(var1, int)

    def test_nodefault_exception_int(self):
        """Test if undefined or value without default raises exception"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getint("test", "var1")
        with self.assertRaises(NoOptionError):
            _ = config.getint("test", "var2")

        config.update_defaults(section="test", defaults={
            "var1": {
                "description": "this is just a test for string var1"
            }
        })

        with self.assertRaises(NoOptionError):
            _ = config.getint("test", "var2")
        with self.assertRaises(NoOptionError):
            _ = config.getint("test", "var1")


class TestFuConfigParserFloat(unittest.TestCase):
    """Test for config wrapper extracting type float"""

    def test_existing_float(self):
        """Test if float variable given in config file is correctly returned"""
        config = FuConfigParser()

        configtext = """
        [test]
        var1 = 10.1
        """
        config.read_string(configtext)

        var1 = config.getfloat("test", "var1")
        print(f"From RawConfigParser -> var1: {var1}")
        self.assertEqual(var1, 10.1)

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": -1.1,
                "description": "this is just a test for float var1"
            }
        })

        var1 = config.getfloat("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertIsInstance(var1, float)
        self.assertEqual(var1, 10.1)

    def test_default_float(self):
        """Test if float variable given in config file is correctly returned"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getfloat("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": -1.1,
                "description": "this is just a test for integer var1"
            }
        })

        var1 = config.getfloat("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertIsInstance(var1, float)
        self.assertEqual(var1, -1.1)

    def test_default_realfloat(self):
        """
        Test if default float variable is returned if given as string

        Note:
        In the configuration file, a float can be given as string,
        so the string should be converted to a float. For the wrapper
        a string value has to work as well.
        """
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getfloat("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": "1.1",
                "description": "this is just a test for float var1"
            }
        })

        var1 = config.getfloat("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertIsInstance(var1, float)
        self.assertEqual(var1, 1.1)

    def test_manual_fallback_float(self):
        """Test if explicit fallback float has precedence"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getfloat("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": -1.1,
                "description": "this is just a test for float var1"
            }
        })

        var1 = config.getfloat("test", "var1", fallback=9.99)
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(var1, 9.99)
        self.assertIsInstance(var1, float)

    def test_nodefault_exception_float(self):
        """Test if undefined or value without default raises exception"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getfloat("test", "var1")
        with self.assertRaises(NoOptionError):
            _ = config.getfloat("test", "var2")

        config.update_defaults(section="test", defaults={
            "var1": {
                "description": "this is just a test for string var1"
            }
        })

        with self.assertRaises(NoOptionError):
            _ = config.getfloat("test", "var2")
        with self.assertRaises(NoOptionError):
            _ = config.getfloat("test", "var1")


class TestFuConfigParserBool(unittest.TestCase):
    """Test for config wrapper extracting type bool"""

    def test_existing_bool(self):
        """Test if float variable given in config file is correctly returned"""
        config = FuConfigParser()

        configtext = """
        [test]
        var1 = true
        """
        config.read_string(configtext)

        var1 = config.getboolean("test", "var1")
        print(f"From RawConfigParser -> var1: {var1}")
        self.assertEqual(var1, True)

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": "false",
                "description": "this is just a test for float var1"
            }
        })

        var1 = config.getboolean("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertIsInstance(var1, bool)
        self.assertEqual(var1, True)

    def test_default_bool(self):
        """
        Test if default boolean variable is returned if given as string

        Note:
        In the configuration file, a boolean has to be given as string,
        so the string should be converted to a boolean.

        """
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getboolean("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": "false",
                "description": "this is just a test for integer var1"
            }
        })

        var1 = config.getboolean("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertIsInstance(var1, bool)
        self.assertEqual(var1, False)

    def test_default_realbool(self):
        """
        Test if default boolean variable is returned if given as bool

        Note:
        In the configuration file, a boolean has to be given as string,
        so the string should be converted to a boolean. For the wrapper
        a boolean value has to work as well.
        """
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getboolean("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": False,
                "description": "this is just a test for integer var1"
            }
        })

        var1 = config.getboolean("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertIsInstance(var1, bool)
        self.assertEqual(var1, False)

    def test_manual_fallback_bool(self):
        """Test if explicit fallback bool has precedence"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getboolean("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": False,
                "description": "this is just a test for bool var1"
            }
        })

        var1 = config.getfloat("test", "var1", fallback=True)
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertTrue(var1)
        self.assertIsInstance(var1, bool)

    def test_nodefault_exception_bool(self):
        """Test if undefined or value without default raises exception"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getboolean("test", "var1")
        with self.assertRaises(NoOptionError):
            _ = config.getboolean("test", "var2")

        config.update_defaults(section="test", defaults={
            "var1": {
                "description": "this is just a test for string var1"
            }
        })

        with self.assertRaises(NoOptionError):
            _ = config.getboolean("test", "var2")
        with self.assertRaises(NoOptionError):
            _ = config.getboolean("test", "var1")


class TestFuConfigParserList(unittest.TestCase):
    """Test for config wrapper extracting list string"""

    def test_existing_list(self):
        """Test if string variable given in config file is correctly returned"""
        config = FuConfigParser()

        configtext = """
        [test]
        var1 = test1,test2
        """
        config.read_string(configtext)

        var1 = config.getlist("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(var1, ["test1", "test2"])

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": "<undefined1>,<undefined2>",
                "description": "this is just a test for string list var1"
            }
        })

        var1 = config.getlist("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(["test1", "test2"], var1)

    def test_default_list(self):
        """Test if default string variable is returned if variable not defined"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getlist("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": "<undefined1>,<undefined2>",
                "description": "this is just a test for string list var1"
            }
        })

        var1 = config.getlist("test", "var1")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(["<undefined1>", "<undefined2>"], var1)

    def test_manual_fallback_list(self):
        """Test if explicit fallback string has precedence"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getlist("test", "var1")

        config.update_defaults(section="test", defaults={
            "var1": {
                "default": "<undefined1>,<undefined2>",
                "description": "this is just a test for string list var1"
            }
        })

        var1 = config.getlist("test", "var1", fallback="{{manual1}},{{manual2}}")
        print(f"From FuConfigParser -> var1: {var1}")
        self.assertEqual(["{{manual1}}", "{{manual2}}"], var1)

    def test_nodefault_exception_list(self):
        """Test if undefined or value without default raises exception"""
        config = FuConfigParser()

        configtext = """
        [test]
        """
        config.read_string(configtext)

        with self.assertRaises(NoOptionError):
            _ = config.getlist("test", "var1")
        with self.assertRaises(NoOptionError):
            _ = config.getlist("test", "var2")

        config.update_defaults(section="test", defaults={
            "var1": {
                "description": "this is just a test for string list var1"
            }
        })

        with self.assertRaises(NoOptionError):
            _ = config.getlist("test", "var2")
        with self.assertRaises(NoOptionError):
            _ = config.getlist("test", "var1")


class TestFuConfigParser(unittest.TestCase):
    """Test cases for fuglu specific config parser"""

    class DummyPlugin(BasicPlugin):
        """A dummy plugin to test replacement of the '${confdir}' marker """

        def __init__(self, config, section=None):
            super().__init__(config, section=section)
            self.requiredvars.update({
                'testdirabs': {
                    'default': '/etc/fuglu/testdirabs',
                    'description': 'test dir with absolute path',
                },
                'testdirrel': {
                    'default': '${confdir}/testdirrel',
                    'description': 'test dir with relative path',
                },
            })

    def test_base(self):
        """ Sinmple - test out=in """
        config = FuConfigParser()
        section = "testsection"
        testvar = "test"
        testvarvalue = "test+123"
        config.add_section(section)
        config.set(section, testvar, testvarvalue)
        outvalue = config.get(section, testvar)
        self.assertEqual(testvarvalue, outvalue, f"Outvalue {outvalue} not equal to value set {testvarvalue}")

    def test_path_setter(self):
        """Check if only path is extracted"""
        config = FuConfigParser()

        path = "/this/is/the/path/to/my/config"
        configfile = "fuglu.conf"
        configfile_with_path = os.path.join(path, configfile)

        config.set_configpath_from_configfile(configfile_with_path)
        self.assertEqual(path, config.markers.get('${confdir}'))

    def test_path_replacement(self):
        """ config string vars with '${confdir} should replace by config file path if set"""
        config = FuConfigParser()
        section = "testsection"
        testvar = "test"
        testvarvalue = "${confdir}/bli/bla/blupp.txt"

        config.add_section(section)
        config.set(section, testvar, testvarvalue)

        path = "/this/is/the/path/to/my/config"
        configfile = "fuglu.conf"
        configfile_with_path = os.path.join(path, configfile)
        config.set_configpath_from_configfile(configfile_with_path)

        # expected value
        expected = testvarvalue.replace("${confdir}", path)
        print(f"expected: {expected}")

        outvalue = config.get(section, testvar)
        print(f"output  : {outvalue}")
        self.assertEqual(expected, outvalue)

    def test_path_replacement_plugin(self):
        """Test path replacement in plugin"""

        config = FuConfigParser()
        config.add_section("test")

        testabsdir = "${confdir}/absdir"
        testreldir = "${confdir}/reldir"

        config.set("test", "testdirabs", testabsdir)
        config.set("test", "testdirrel", testreldir)

        path = "/this/is/the/path/to/my/config"
        configfile = "fuglu.conf"
        configfile_with_path = os.path.join(path, configfile)
        config.set_configpath_from_configfile(configfile_with_path)

        d = TestFuConfigParser.DummyPlugin(config=config, section="test")
        outdirabs = d.config.get(d.section, "testdirabs")
        outdirrel = d.config.get(d.section, "testdirrel")

        print(f"out(abs): {outdirabs}")
        print(f"out(rel): {outdirrel}")

        self.assertEqual(testabsdir.replace("${confdir}", path), outdirabs)
        self.assertEqual(testreldir.replace("${confdir}", path), outdirrel)

    def test_path_default_replacement_plugin(self):
        """Test path replacement for default config values in plugin"""

        config = FuConfigParser()
        config.add_section("test")

        testabsdir = '/etc/fuglu/testdirabs'
        testreldir = '${confdir}/testdirrel'

        path = "/this/is/the/path/to/my/config"
        configfile = "fuglu.conf"
        configfile_with_path = os.path.join(path, configfile)
        config.set_configpath_from_configfile(configfile_with_path)

        d = TestFuConfigParser.DummyPlugin(config=config, section="test")
        outdirabs = d.config.get(d.section, "testdirabs")
        outdirrel = d.config.get(d.section, "testdirrel")

        print(f"out(abs): {outdirabs}")
        print(f"out(rel): {outdirrel}")

        self.assertEqual(testabsdir.replace("${confdir}", path), outdirabs)
        self.assertEqual(testreldir.replace("${confdir}", path), outdirrel)

    def test_path_nodefault_replacement_plugin(self):
        """With no config path set, the fuglu default /etc/fuglu should be used"""
        config = FuConfigParser()
        config.add_section("test")

        testabsdir = '/etc/fuglu/testdirabs'
        testreldir = '${confdir}/testdirrel'

        d = TestFuConfigParser.DummyPlugin(config=config, section="test")
        outdirabs = d.config.get(d.section, "testdirabs")
        outdirrel = d.config.get(d.section, "testdirrel")

        print(f"out(abs): {outdirabs}")
        print(f"out(rel): {outdirrel}")

        self.assertEqual(testabsdir.replace('${confdir}', '/etc/fuglu'), outdirabs)
        self.assertEqual(testreldir.replace('${confdir}', '/etc/fuglu'), outdirrel)


class TestFuConfigParserOtherSec(unittest.TestCase):
    """Test cases for fuglu specific config parser with requiredvars in other sections"""

    class DummyPlugin(BasicPlugin):
        """A dummy plugin to test using default value from other section """

        def __init__(self, config, section=None):
            super().__init__(config, section=section)
            self.requiredvars.update({
                'test1': {
                    'default': 'local',
                    'description': 'test in plugin',
                },
                'test2': {
                    'default': 'main',
                    'description': 'test in main section',
                    'section': "main"
                },
            })

    def test_with_othersection(self):
        """ Base testcase with 2 sections defined in DummyPlugin """
        config = FuConfigParser()
        config.add_section("DummyPlugin")
        config.add_section("main")
        dplug = TestFuConfigParserOtherSec.DummyPlugin(config=config)
        self.assertEqual("local", dplug.config.get("DummyPlugin", 'test1'))
        self.assertEqual("main", dplug.config.get("main", 'test2'))

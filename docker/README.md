

# build the images
```
docker compose rm clamd fuglud spamd redisd
docker compose build --no-cache
```

# start the services
```
docker compose up
or to the background
docker compose up -d
```

# stop the services
```
docker compose down
or
ctrl-c if you run in foreground
docker compose down
```

# redeploy
```
docker compose build --no-cache
docker compose down && docker compose up -d
```

# reference
https://docs.docker.com/compose/gettingstarted/
https://www.educative.io/blog/docker-compose-tutorial
https://devhints.io/docker-compose

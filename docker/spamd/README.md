# build
call build.sh or issue manual command such as following
```
$ docker build -t spamd_img -f Dockerfile  .
Sending build context to Docker daemon  2.048kB
Step 1/5 : ARG BASEIMAGE=debian:stable-slim
Step 2/5 : FROM $BASEIMAGE as release
 ---> 879faceb20f7
Step 3/5 : RUN apt update && apt -y install spamassassin && apt clean && rm -rf /var/lib/apt/lists
 ---> Running in 7856b73e1201

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

Get:1 http://deb.debian.org/debian stable InRelease [151 kB]
Get:2 http://deb.debian.org/debian stable-updates InRelease [52.1 kB]
Get:3 http://deb.debian.org/debian-security stable-security InRele
...
...
...
Step 4/5 : CMD spamd --nouser-config   --syslog stderr   --helper-home-dir /var/lib/spamassassin   --ip-address   --allowed-ips 0.0.0.0/0
 ---> Running in 6803d553280c
Removing intermediate container 6803d553280c
 ---> be2ac799235d
Step 5/5 : EXPOSE 783
 ---> Running in 87fb28ef121b
Removing intermediate container 87fb28ef121b
 ---> 637e1feb02a2
Successfully built 637e1feb02a2
Successfully tagged spamd_img:latest
```

# check built image
```
$ docker images | grep spamd
spamd_img                                       latest        637e1feb02a2   About a minute ago   461MB
```

# start spamd as container
```
$ docker run  -p 783:783 -d  --name spamd spamd_img
$ docker ps | grep spamd
86bc5b22c556   spamd_img                                                   "/bin/sh -c 'spamd -…"   5 seconds ago   Up 4 seconds   783/tcp        
```

# check everything okay
```
$ docker logs spamd
Jan 25 13:01:39.446 [7] info: zoom: able to use 388/388 'body_0' compiled rules (100%)
Jan 25 13:01:39.678 [7] info: spamd: server started on IO::Socket::IP [0.0.0.0]:783 (running version 4.0.0)
Jan 25 13:01:39.679 [7] info: spamd: server pid: 7
Jan 25 13:01:39.680 [7] info: spamd: server successfully spawned child process, pid 9
Jan 25 13:01:39.682 [7] info: spamd: server successfully spawned child process, pid 10
Jan 25 13:01:39.682 [7] info: prefork: child states: IS
Jan 25 13:01:39.683 [7] info: prefork: child states: II
```

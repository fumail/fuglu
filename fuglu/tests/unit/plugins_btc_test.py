# -*- coding: UTF-8 -*-
import unittest
import logging
import sys
import os
from fuglu.asyncprocpool import get_event_loop
from fuglu.stringencode import force_uString
from fuglu.plugins.btc import BTCCounter
from fuglu.shared import Suspect, FuConfigParser
from fuglu.extensions.filearchives import PYPDF_AVAILABLE
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from .unittestsetup import TESTDATADIR


def setup_module():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)


def get_config():
    confdefaults = b"""
[test]
redisdb = redis://127.0.0.1:6379/1
btcblacklistfile = ''
"""
    conf = FuConfigParser()
    conf.read_string(force_uString(confdefaults))
    return conf


BTC_MAIL = [
    ('btc_html_b64.eml', '12yCNJHAwda8Kgxv9DswpS9k16XnstSqcJ'),
    ('btc_scrambled.eml', '1A5Kr9Up2sMp3fWeVVrzTHPEfvoinCGE6c'),
    ('btc_scrambled2.eml', '13KRkTwEvd8suAjK389Qu28XWLCsPXvNKj'),
    ('btc_regular_b64.eml', '1M3r1T2MnqRPJUgWoSZTgaTqpVQUNdkpeC'),
    ('btc_regular_plain.eml', '18p86MxfVKdTn5FhxU97WHJ3af1fHpCnfn'),
    ('btc_scrambled3.eml', '1JTGgvEBtXfveMGhgHFKbY6YSVJs5vvscd'),
    ('btc_bc1.eml', 'bc1q7y8nhq6j237hcdup0ve2vatcrxc2x9uwc5d4tz'),
]


class TestBTCMixing(unittest.TestCase):
    def test_extract_all_btc(self):
        for filename, btcaddr in BTC_MAIL:
            self._test_extract_btc(filename, btcaddr)


    def _test_extract_btc(self, filename, btcaddr):
        conf = get_config()

        filename = os.path.join(TESTDATADIR, filename)
        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org',filename)

        btcm = BTCCounter(conf, 'test')
        btcaddrs = btcm.extract(suspect, 1280000)

        #print(btcaddrs)
        self.assertIn(btcaddr, btcaddrs, 'BTC address %s not found in result' % btcaddr)
    
    
    def test_extract_pdf(self):
        if PYPDF_AVAILABLE:
            msg = MIMEMultipart()
            msg['From'] = 'sender@unittests.fuglu.org'
            msg['To'] = 'recipient@unittests.fuglu.org'
            
            filename = os.path.join(TESTDATADIR, 'btc_in_text.pdf')
            with open(filename, 'rb') as f:
                p = MIMEApplication(f.read() ,_subtype="pdf")
            p.add_header('Content-Disposition','attachment',filename='btc_in_text.pdf')
            msg.attach(p)
            
            conf = get_config()
            suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
            suspect.set_message_rep(msg)
            btcm = BTCCounter(conf, 'test')
            btcaddrs = btcm.extract(suspect, len(msg.as_string()))
            self.assertIn('bc1qz2qfcpyd9kj5vqxc0xk2vcfqxsjz0v6tnapacp', btcaddrs)
            
            
    
    
    def test_set_get(self):
        key = 'bc1q7y8nhq6j237hcdup0ve2vatcrxc2x9uwc5d4tz'
        config = FuConfigParser()
        section = 'BTCTest'
        config.add_section(section)
        config.set(section, 'redis_conn', 'redis://redis:6379/1')
        suspect = Suspect('sender@example.fuglu.org', 'recipient@unittests.fuglu.org', os.path.join(TESTDATADIR, 'btc_bc1.eml'))
        event_loop = get_event_loop()
        candidate = BTCCounter(config, section)
        candidate._init_backend_redis()
        event_loop.run_until_complete(candidate.backend_redis.reset(key)) # cleanup possible leftovers from previous runs
        level, btcaddr = candidate._get_count(suspect, [key], [])
        self.assertEqual(level, -2)
        self.assertEqual(btcaddr, None)
        candidate.report(suspect)
        level, btcaddr = candidate._get_count(suspect, [key], [])
        self.assertEqual(level, 1)
        self.assertEqual(btcaddr, key)
        level, btcaddr = candidate._get_count(suspect, [key], [key]) # test with skiplist
        self.assertEqual(level, -2)
        self.assertEqual(btcaddr, None)
        val = event_loop.run_until_complete(candidate.backend_redis.reset(key)) # final cleanup
        self.assertEqual(val, 1, f'failed to delete key {key}')

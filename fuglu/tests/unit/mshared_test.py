# -*- coding: UTF-8 -*-
import unittest
import os
import sys
import typing as tp
from configparser import ConfigParser
from unittest.mock import MagicMock

# PyCharm issue (not loading path correctly, only in debug mode)
UNITTESTDIR = os.path.dirname(os.path.realpath(__file__))
if UNITTESTDIR not in sys.path:
    sys.path.insert(0, UNITTESTDIR)

from fuglu.mshared import BasicMilterPlugin, BMPConnectMixin, BMPHeloMixin
import fuglu.connectors.milterconnector as sm
import fuglu.connectors.asyncmilterconnector as asm


class TestMilterPluginOverride(unittest.TestCase):
    """Test return code override option for milter plugins"""

    class DummyMilterPlugin(BMPHeloMixin, BMPConnectMixin, BasicMilterPlugin):
        """A dummy milter plugin"""
        def __init__(self, config, section=None):
            super().__init__(config, section=section)

        def examine_connect(self,
                            sess: tp.Union[sm.MilterSession, asm.MilterSession],
                            host: str, addr: str
                            ) -> tp.Union[bytes, tp.Tuple[bytes, str]]:
            return sm.REJECT

        def examine_helo(self,
                         sess: tp.Union[sm.MilterSession, asm.MilterSession],
                         helo: str
                         ) -> tp.Union[bytes, tp.Tuple[bytes, str]]:
            return sm.TEMPFAIL, "tempfail"

    def test_reject(self):
        """Test reject of dummy milter plugin"""
        conf = ConfigParser()
        conf.add_section('test')

        plug = TestMilterPluginOverride.DummyMilterPlugin(conf, section="test")
        sess = asm.MilterSession(MagicMock(), MagicMock())
        sess.ptr = "aaaaa.aaa.aaa.aaaaaaaaaa.domain.invalid"
        sess.addr = "192.168.1.1"
        res = plug.examine_connect(sess=sess, host=sess.ptr, addr=sess.addr)
        res = plug._check_apply_override_milter(res, self.id)
        self.assertEqual(res, sm.REJECT)

    def test_reject_override(self):
        """Test reject override of dummy milter plugin"""
        conf = ConfigParser()
        conf.add_section('test')
        conf.set('test', 'overrideaction', 'dunno')

        plug = TestMilterPluginOverride.DummyMilterPlugin(conf, section="test")
        sess = asm.MilterSession(MagicMock(), MagicMock())
        sess.ptr = "aaaaa.aaa.aaa.aaaaaaaaaa.domain.invalid"
        sess.addr = "192.168.1.1"
        res = plug.examine_connect(sess=sess, host=sess.ptr, addr=sess.addr)
        res = plug._check_apply_override_milter(res, self.id)
        self.assertEqual(res, sm.CONTINUE)

    def test_tempfail_msg(self):
        """Test tempfail of dummy milter plugin with message"""
        conf = ConfigParser()
        conf.add_section('test')

        plug = TestMilterPluginOverride.DummyMilterPlugin(conf, section="test")
        sess = asm.MilterSession(MagicMock(), MagicMock())
        sess.ptr = "aaaaa.aaa.aaa.aaaaaaaaaa.domain.invalid"
        sess.addr = "192.168.1.1"
        sess.heloname = "helo.domain.invalid"
        res = plug.examine_helo(sess=sess, helo=sess.heloname)
        res = plug._check_apply_override_milter(res, self.id)
        self.assertEqual(res, (sm.TEMPFAIL, "tempfail"))

    def test_tempfail_msg_override(self):
        """Test tempfail override of dummy milter plugin with message"""
        conf = ConfigParser()
        conf.add_section('test')
        conf.set('test', 'overrideaction', 'reject')
        conf.set('test', 'overridemessage', 'reject')

        plug = TestMilterPluginOverride.DummyMilterPlugin(conf, section="test")
        sess = asm.MilterSession(MagicMock(), MagicMock())
        sess.ptr = "aaaaa.aaa.aaa.aaaaaaaaaa.domain.invalid"
        sess.addr = "192.168.1.1"
        sess.heloname = "helo.domain.invalid"
        res = plug.examine_helo(sess=sess, helo=sess.heloname)
        res = plug._check_apply_override_milter(res, self.id)
        self.assertEqual(res, (sm.REJECT, "reject"))

    def test_strip_address_cleanup(self):
        """Test address cleanup"""
        addresses = [b"sender@unittests.fuglu.org", b"recipient1@unittest.fuglu.org", b"recipient@unittest.fuglu.org"]
        sess = sm.MilterSession(MagicMock())
        for addr in addresses:
            modwithspaces = b"<  " + addr + b"  >"
            self.assertEqual(addr, sess._clean_address(modwithspaces))

    def test_strip_address_cleanup_async(self):
        """Test address cleanup (async milter)"""
        sess = asm.MilterSession(MagicMock(), MagicMock())
        addresses = [b"sender@unittests.fuglu.org", b"recipient1@unittest.fuglu.org", b"recipient@unittest.fuglu.org"]
        for addr in addresses:
            modwithspaces = b"<  " + addr + b"  >"
            self.assertEqual(addr, sess._clean_address(modwithspaces))

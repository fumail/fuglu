# -*- coding: UTF-8 -*-
import unittest
from fuglu.shared import Suspect, FuConfigParser
from fuglu.plugins.decision import StripSubjectTag
from .unittestsetup import TESTDATADIR
import os

class StripSubjectTagTestCase(unittest.TestCase):
    """
    Test decision.StripSubjectTag plugin
    """
    def setUp(self):
        config = FuConfigParser()
        config.add_section('StripSubjectTag')
        self.candidate = StripSubjectTag(config)

    def tearDown(self):
        pass
    
    def test_lint(self):
        """
        Run lint test
        """
        result = self.candidate.lint()
        self.assertTrue(result)
    
    def test_noop(self):
        """
        test that no change happens when no config tag is set
        """
        subject = '[EXT] this is a sample subject'
        filename = os.path.join(TESTDATADIR, 'helloworld.eml')
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', filename)
        suspect.set_header('subject', subject)
        removetags = self.candidate.config.getlist(self.candidate.section, 'removetags')
        for tagname in removetags:
            subject_tag_spam = self.candidate._get_filtersetting(suspect, tagname)
            self.assertIsNone(subject_tag_spam, f'tag value for tag name {tagname} is {subject_tag_spam} expected None')
        self.candidate.examine(suspect)
        self.assertEqual(suspect.get_header('subject'), subject)
    
    def test_strip_full(self):
        """
        test that subject tag is completely removed for previously correctly set tags
        """
        tagvalue = '[EXT]'
        subjects = [
            ('[EXT] this is a sample subject', 'this is a sample subject'),
            ('Re: [EXT] this is a sample subject', 'Re: this is a sample subject'),
            ('Re:[EXT] this is a sample subject', 'Re:this is a sample subject'),
            ('Re:[EXT] [EXT] this is a sample subject', 'Re:this is a sample subject'),
        ]
        
        filename = os.path.join(TESTDATADIR, 'helloworld.eml')
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', filename)
        suspect.set_tag('filtersettings', {'subject_tag_ext': tagvalue})
        for subject, exp in subjects:
            suspect.set_header('subject', subject)
            self.candidate.examine(suspect)
            self.assertEqual(suspect.get_header('subject'), exp, 'subject not equal')
            self.assertNotIn(tagvalue, suspect.get_header('subject'), f"tag value {tagvalue} found in subject {suspect.get_header('subject')}")
    
    def test_strip_incomplete(self):
        """
        test that subject tag is only partially removed for previously partially incorrectly set tags (e.g. no space after tag)
        """
        tagvalue = '[EXT]'
        subjects = [
            ('Re:[EXT] [EXT]this is a sample subject', 'Re:[EXT]this is a sample subject'),
        ]
        
        suspect = Suspect('sender@unittests.fuglu.org', 'rcpt@unittests.fuglu.org', '/dev/null')
        suspect.set_tag('filtersettings', {'subject_tag_ext': tagvalue})
        for subject, exp in subjects:
            suspect.set_header('subject', subject)
            self.candidate.examine(suspect)
            self.assertEqual(suspect.get_header('subject'), exp)
    
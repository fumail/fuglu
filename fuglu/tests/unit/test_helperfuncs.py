import unittest
import unittest.mock

from fuglu.plugins.ratelimit.helperfuncs import get_skip_ptr_unknown, get_ptr
from fuglu.extensions.dnsquery import FuSERVFAIL


class TestGetSkipPtrUnknown(unittest.TestCase):
    """Tests for get_skip_ptr_unknown helper function"""

    def test_return(self) -> None:
        """return same value if input is not 'unknown'"""
        self.assertEqual("ptr", get_skip_ptr_unknown("ptr"))

    def test_none(self) -> None:
        """return same value if input is not 'unknown'"""
        self.assertIsNone(get_skip_ptr_unknown("unknown"))



class TestGetPtr(unittest.TestCase):
    """Tests for get_ptr helper function"""

    @staticmethod
    def _raise_servfail(*args, **kwargs):
        raise FuSERVFAIL()

    def test_servfail(self) -> None:
        """Test a servfail returns 'unknown'"""
        with unittest.mock.patch("fuglu.extensions.dnsquery.revlookup", side_effect=TestGetPtr._raise_servfail) as revlookupmock:
            ptr = get_ptr("127.0.0.1")
        self.assertEqual("unknown", ptr)
        revlookupmock.assert_called_once()
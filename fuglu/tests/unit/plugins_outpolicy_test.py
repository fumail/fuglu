# -*- coding: UTF-8 -*-
from .unittestsetup import TESTDATADIR
import unittest
import os
from fuglu.shared import Suspect, FuConfigParser, DUNNO, REJECT
from fuglu.plugins.outpolicy import FuzorRateLimit, NoBounce, ToCCLimit, FromNameCheck
from fuglu.stringencode import force_bString
import fuglu.connectors.milterconnector as sm
import tempfile
import random
import string
from unittest.mock import MagicMock


class TestFuzorRateLimit(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.tmpfile_sender = tempfile.NamedTemporaryFile()
        cls.tmpfile_alert = tempfile.NamedTemporaryFile()
        confdefaults = f"""
        [test]
        sender_exception_file = {cls.tmpfile_sender.name}
        alert_exception_file = {cls.tmpfile_alert.name}
        """
        conf = FuConfigParser()
        conf.read_string(confdefaults)
        cls.config = conf

    def test_nosubject(self,):
        """
        Ignore empty subject keyword check for empty subjects
        """

        tmpfile_sender = tempfile.NamedTemporaryFile()
        tmpfile_alert = tempfile.NamedTemporaryFile()
        # use special config because threshold has to be 0 such that
        # because otherwise the subject is not used for testing keywords
        confdefaults = f"""
        [test]
        sender_exception_file = {tmpfile_sender.name}
        alert_exception_file = {tmpfile_alert.name}
        threshold = 0
        maxsize = 600000
        demomode = False
        actioncode = REJECT
        rejectmessage =
        """
        conf = FuConfigParser()
        conf.read_string(confdefaults)

        filename = os.path.join(TESTDATADIR, "subject_umlaut.eml")

        suspect = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', filename)
        msg = suspect.get_message_rep()
        del msg["Subject"]
        self.assertNotIn("Subject", msg.keys())

        suspect.set_source(msg.as_bytes(), att_mgr_reset=True)

        frl = FuzorRateLimit(conf, "test")
        frl.examine(suspect)

    def test_lint(self,):
        confdefaults = "[test]"
        conf = FuConfigParser()
        conf.read_string(confdefaults)

        frl = FuzorRateLimit(self.config, "test")
        out = frl.lint()
        print(out)



class MiniSess(object):
    tags = {'dnsdata.recipient.example.com': {'MX': ['mx1.example.com', 'mx2.example.com'], 'MXA': ['1.1.1.1', '2.2.2.2']}}

class TestNoBounce(unittest.TestCase):
    def setUp(self) -> None:
        self.nobouncefile = tempfile.NamedTemporaryFile()
        self.nobouncemxfile = tempfile.NamedTemporaryFile()
        
        nobounce = ['example.com']
        with open(self.nobouncefile.name, 'w') as f:
            f.writelines(nobounce)
            
        nobouncemx = ['mx1.example.com']
        with open(self.nobouncemxfile.name, 'w') as f:
            f.writelines(nobouncemx)
        
        confdefaults = f"""
        [test]
        nobouncefile = {self.nobouncefile.name}
        nobounce_mx_file = {self.nobouncemxfile.name}
        """
        conf = FuConfigParser()
        conf.read_string(confdefaults)
        self.config = conf
    
    def test_nobounce(self):
        candidate = NoBounce(self.config, "test")
        candidate._init_lists()
        nobounce = set(candidate.nobounce.get_list())
        
        self.assertIn('example.com', nobounce, 'example.com not found in nobounce list')
        self.assertNotIn('example.org', nobounce, 'example.org not found in nobounce list')
    
    def test_nobounce_mx(self):
        candidate = NoBounce(self.config, "test")
        candidate._init_lists()
        sess = MiniSess()
        
        mx = candidate._check_nobounce_mx(sess, 'example.com')
        self.assertIn(mx, ['mx1.example.com', 'mx2.example.com'], f'{mx} not found in mx list')
        mx = candidate._check_nobounce_mx(sess, 'example.org')
        self.assertIsNone(mx, f'spurious {mx} found in mx list')


class TestToCCLimit(unittest.TestCase):
    def get_suspect(self):
        source = b'''From: Test Sender <sender@unittests.fuglu.org>
Subject: Just a test
To: To One <torcpt1@unittests.fuglu.org>, To Two <torcpt2@unittests.fuglu.org>
CC:  CC One <ccrcpt1@unittests.fuglu.org>, CC Two <ccrcpt2@unittests.fuglu.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8

Lorem Ipsum
'''
        s = Suspect('sender@unittests.fuglu.org', 'recipient@unittests.fuglu.org', '/dev/null')
        s.set_source(source)
        return s
    
    def test_lint(self):
        conf = FuConfigParser()
        candidate = ToCCLimit(conf, "test")
        result = candidate.lint()
        self.assertTrue(result)
        
    def test_nolimit(self):
        conf = FuConfigParser()
        conf.add_section('test')
        candidate = ToCCLimit(conf, "test")
        action, message = candidate.examine(self.get_suspect())
        self.assertEqual(action, DUNNO, 'not accepting')
    
    def test_limit_to(self):
        conf = FuConfigParser()
        conf.add_section('test')
        conf.set('test', 'max_to', '1')
        candidate = ToCCLimit(conf, "test")
        action, message = candidate.examine(self.get_suspect())
        self.assertEqual(action, REJECT, 'not rejecting')
    
    def test_hilimit_to(self):
        conf = FuConfigParser()
        conf.add_section('test')
        conf.set('test', 'max_to', '2')
        candidate = ToCCLimit(conf, "test")
        action, message = candidate.examine(self.get_suspect())
        self.assertEqual(action, DUNNO, 'not accepting')
    
    def test_limit_cc(self):
        conf = FuConfigParser()
        conf.add_section('test')
        conf.set('test', 'max_cc', '1')
        candidate = ToCCLimit(conf, "test")
        action, message = candidate.examine(self.get_suspect())
        self.assertEqual(action, REJECT, 'not rejecting')
    
    def test_hilimit_cc(self):
        conf = FuConfigParser()
        conf.add_section('test')
        conf.set('test', 'max_cc', '2')
        candidate = ToCCLimit(conf, "test")
        action, message = candidate.examine(self.get_suspect())
        self.assertEqual(action, DUNNO, 'not accepting')
    
    def test_limit_rcpt(self):
        conf = FuConfigParser()
        conf.add_section('test')
        conf.set('test', 'max_rcpt', '3')
        candidate = ToCCLimit(conf, "test")
        action, message = candidate.examine(self.get_suspect())
        self.assertEqual(action, REJECT, 'not rejecting')
    
    def test_hilimit_rcpt(self):
        conf = FuConfigParser()
        conf.add_section('test')
        conf.set('test', 'max_rcpt', '4')
        candidate = ToCCLimit(conf, "test")
        action, message = candidate.examine(self.get_suspect())
        self.assertEqual(action, DUNNO, 'not accepting')


class TestFromNameCheck(unittest.TestCase):
    def randomstring(self, length):
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(length))
    
    def setUp(self) -> None:
        config=FuConfigParser()
        configfile ="""
[FromNameCheck]
redis_conn=redis://redis:6379/1
redis_ttl=300
redis_timeout=10
threshold=1
        """
        config.read_string(configfile)
        self.candidate = FromNameCheck(config)
        self.candidate._init_data()
        self.targetaddress = f'sender{self.randomstring(5)}@unittests.fuglu.org'
        
    def tearDown(self):
        redisconn = self.candidate.backend_redis.redis_pool.get_conn()
        redisconn.delete(self.targetaddress)
    
    def test_lint(self):
        result = self.candidate.lint()
        self.assertTrue(result, 'Lint failed')
    
    def test_namechange(self):
        """Test the redis backend"""
        self.assertIsNotNone(self.candidate.backend_redis)
        count = self.candidate.backend_redis.check_update_name(self.targetaddress, 'Test1')
        self.assertEqual(count,1, f'wrong count {count}!=1 after initial test')
        count = self.candidate.backend_redis.check_update_name(self.targetaddress, 'Test1')
        self.assertEqual(count,1, f'wrong count {count}!=1 after test without name change')
        count = self.candidate.backend_redis.check_update_name(self.targetaddress, 'Test2')
        self.assertEqual(count,2, f'wrong count {count}!=2 after test with name change')
    
    def test_hdrextract(self):
        """Test header value extraction"""
        values = {
            b'': (None, None),
            b'asdf@unittests.fuglu.org': ('', 'asdf@unittests.fuglu.org'),
            b'<asdf@unittests.fuglu.org>': ('', 'asdf@unittests.fuglu.org'),
            b'"User Name" <asdf@unittests.fuglu.org>': ('User Name', 'asdf@unittests.fuglu.org'),
            b'User Name <asdf@unittests.fuglu.org>': ('User Name', 'asdf@unittests.fuglu.org'),
        }
        for value in values:
            hdrname, hdrfrom = self.candidate._hdr_extract(value)
            expect = values[value]
            self.assertEqual(hdrname, expect[0], f'failed to extract name from {value} got {hdrname} expected {expect[0]}')
            self.assertEqual(hdrfrom, expect[1], f'failed to extract addr from {value} got {hdrfrom} expected {expect[1]}')
        
    def test_examine(self):
        """Test the entire plugin"""
        sess = sm.MilterSession(MagicMock(), MagicMock())
        sess.recipients = [b'asdf@unittests.fuglu.org']
        
        value = force_bString(f'"Test1" <{self.targetaddress}>')
        reply = self.candidate.examine_header(sess, b'from', value)
        self.assertEqual(reply, sm.CONTINUE, f'wrong response code after initial test')
        reply = self.candidate.examine_header(sess, b'from', value)
        self.assertEqual(reply, sm.CONTINUE, f'wrong response code after test without name change')
        
        value = force_bString(f'"Test2" <{self.targetaddress}>')
        reply = self.candidate.examine_header(sess, b'from', value)
        self.assertEqual(len(reply), 2, f'wrong response type after test with name change')
        self.assertEqual(reply[0], sm.REJECT, f'wrong response code after test with name change')


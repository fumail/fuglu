# -*- coding: utf-8 -*-
import unittest
import tempfile
from fuglu.plugins.p_skipper import PluginSkipper
from fuglu.plugins.decision import KillerPlugin
from fuglu.shared import Suspect, FuConfigParser


class PluginSkipperTest(unittest.TestCase):

    def test_recipient(self):
        regexfile = tempfile.NamedTemporaryFile(suffix='skiplist', prefix='fuglu-unittest', dir='/tmp')

        regexfile.write(r"""
envelope_to /^skipclam@fuglu\.org$/   KillerPlugin
        """.encode())
        regexfile.seek(0)  #  makes sure file has been written

        config = FuConfigParser()
        config.add_section('PluginSkipper')
        config.set('PluginSkipper', 'filterfile', regexfile.name)
        pskipper = PluginSkipper(config=config)

        suspect_noskip = Suspect("sender@fuglu.org", "noskip@fuglu.org", "/dev/null")
        suspect_skip = Suspect("sender@fuglu.org", "skipclam@fuglu.org", "/dev/null")

        pluginlist = [KillerPlugin(config=config, section="Test")]

        listout = pskipper.pluginlist(suspect=suspect_noskip, pluginlist=pluginlist)
        self.assertEqual(pluginlist, listout, "KillerPlugin should remain!")

        listout = pskipper.pluginlist(suspect=suspect_skip, pluginlist=pluginlist)
        self.assertEqual([], listout, "KillerPlugin should get removed...")


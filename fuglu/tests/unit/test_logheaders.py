import unittest
import os
import sys
from unittest.mock import patch, MagicMock

# PyCharm issue (not loading path correctly, only in debug mode)
UNITTESTDIR = os.path.dirname(os.path.realpath(__file__))
if UNITTESTDIR not in sys.path:
    sys.path.insert(0, UNITTESTDIR)

from .unittestsetup import TESTDATADIR

from fuglu.shared import Suspect, FuConfigParser
from fuglu.plugins.logheaders import LogHeaders


class TestLogLevelSuspect(unittest.TestCase):
    """LogHeaders plugin tests using suspect"""

    def test_base(self) -> None:
        """Base test - no exception"""
        confdefaults = """
        [test]
        """
        conf = FuConfigParser()
        conf.read_string(confdefaults)
        plg = LogHeaders(config=conf, section="test")

        filename = os.path.join(TESTDATADIR, 'helloworld.eml')
        suspect = Suspect("from@fuglu.test", "to@fuglu.test", filename)
        
        plg.examine(suspect=suspect)

    def test_count(self) -> None:
        """Check log calls are equal to number of headers"""
        confdefaults = """
        [test]
        """
        conf = FuConfigParser()
        conf.read_string(confdefaults)
        plg = LogHeaders(config=conf, section="test")
        plg.logger = MagicMock()

        filename = os.path.join(TESTDATADIR, 'helloworld.eml')
        suspect = Suspect("from@fuglu.test", "to@fuglu.test", filename)

        # number of headers in helloworld message
        nheaders = len(suspect.get_message_rep())
        
        plg.examine(suspect=suspect)
        self.assertEqual(nheaders, plg.logger.log.call_count, f"The helloworld mail has {nheaders} headers, each should trigger a log message!")